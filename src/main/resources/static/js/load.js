function loadbg() {
    const bg = localStorage.getItem("bg");
    const upload_tip = document.getElementById("upload-tip");
    const bgimg = document.getElementById("bgimg");
    const img_show = document.getElementById("img-show");
    const oneword = document.getElementById("oneword");
    const footpower = document.getElementById("footpower");
    const footMRyan = document.getElementById("footMRyan");
    const weather = document.getElementById("weather-float-he");
    const list = document.getElementById("list");
    if (bg != null) {
        img_show.src = bg;
        bgimg.style.display = "block";
        bgimg.src = bg;
        bgimg.style.display = "block";
        img_show.style.display = "inline";
        upload_tip.style.display = "none";
        oneword.style.color = "rgba(255,255,255,0.75)"
        footpower.style.color = "rgba(255,255,255,0.58)"
        footMRyan.style.color = "rgba(255,255,255,0.75)"
        list.style.background = "rgba(255,255,255,0.27)"
    }
    weather.style.position = "absolute";
    weather.style.zIndex = "100";
}

function imgfileChange() {
    const upload_tip = document.getElementById("upload-tip");
    const bgimg = document.getElementById("bgimg");
    const img_show = document.getElementById("img-show");
    const imgfile = document.getElementById("imgfile");
    const oneword = document.getElementById("oneword");
    const footpower = document.getElementById("footpower");
    const footMRyan = document.getElementById("footMRyan");
    const weather = document.getElementById("weather-float-he");
    const list = document.getElementById("list");
    var freader = new FileReader();
    //开始读取指定的Blob中的内容。一旦完成，result属性中将包含一个data: URL格式的Base64字符串以表示所读取文件的内容。
    freader.readAsDataURL(imgfile.files[0]);
    /*       console.log(imgfile.files[0])*/
    //处理load事件。该事件在读取操作完成时触发。
    freader.onload = function (e) {
        //  e.target.result;  就是data:image/jpeg;base64,+编码  就相当于直接解码
        img_show.src = e.target.result;
        localStorage.setItem("bg", e.target.result + "");
        bgimg.style.display = "block";
        bgimg.src = e.target.result;
        /*  console.log(e.target.result)*/
        img_show.style.display = "inline";
        upload_tip.style.display = "none";
        oneword.style.color = "rgba(255,255,255,0.75)"
        footpower.style.color = "rgba(255,255,255,0.75)"
        footMRyan.style.color = "rgba(255,255,255,0.75)"
        list.style.background = "rgba(255,255,255,0.27)"
        weather.style.position = "absolute";
        weather.style.zIndex = "100";
    };

}

<!--网站标题自动判断	设置/*-->
var title = document.title;
// window 失去焦点
window.onblur = function () {
    document.title = '你已经离开了一会';
};
// window 获得焦点
window.onfocus = function () {
    document.title = 'MRyan搜索';
    setTimeout("document.title=title", 3000);

}
window.onload = function () {
    loadbg()
}
