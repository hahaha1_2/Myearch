package com.mryansearch.demo.Constroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @description：TODO
 * @Author MRyan
 * @Date 2020/4/13 21:11
 * @Version 1.0
 */
@Controller
public class IndexController {

    @GetMapping("/")
    public String index() {
        return "searchindex";
    }
}
