# 面向搜索引擎编程工具

#### 介绍
面向搜索引擎编程工具


### 项目简介

面向搜索引擎编程那是个玩笑话。
目的是更方便的搜索学习知识。

 **话不多说** 

看了项目截图你就知道这是个什么工具了。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0616/091730_486c9200_2353571.png "界面美观")

 **一个搜索页面，可以设置成浏览器默认主页** 

![输入图片说明](https://images.gitee.com/uploads/images/2020/0616/091730_ed143f46_2353571.png "界面美观")

 **自定义收藏夹** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0616/091730_18a5de8b_2353571.png "界面美观")

 **多种搜索方式** 

![输入图片说明](https://images.gitee.com/uploads/images/2020/0616/091730_f95f53b6_2353571.png "界面美观")

 **可自定义背景** 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0930/154315_e3a95f7d_2353571.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0930/154315_3a968800_2353571.png "屏幕截图.png")

适配多端：

![输入图片说明](https://images.gitee.com/uploads/images/2020/0616/091918_82d6b467_2353571.jpeg "界面美观")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0616/091958_06de247b_2353571.jpeg "界面美观")

### 如何使用

1.  **直接使用：** 如果你嫌麻烦可以直接访问我的地址 将网页设置为主页
[我的网址](http://www.mryan.xyz:666/)

2.  **自定义：** 或者先给个star，然后clone项目
对代码进行修改：
**修改位置：searchindex.html** 
端口号：666

你完全可以自定制成自己想要的样子

### 动机
网络收藏夹有浏览器限制，市面上的类似产品广告多，同时也不可定制，那怎么能又美观又使用，于是它出现了。


 **仅供学术交流，便于自己便于大家。** 

如果觉得好用，请给个Star

项目地址：[地址](https://gitee.com/Ryan_ma/Myearch)








